import React from "react";
import "./App.css";
import Greeting from "./Greeting";
import Clock from "./Clock";
import UserProfile from "./Profile/UserProfile";

function App() {
  return (
    <>
      <div className="App">
        <div className="App-header">
          <UserProfile/>
          <Greeting name="Mohammad Nanda Rizky" age="22" gender="male" />
          <Clock />
        </div>
      </div>
    </>
  );
}

export default App;
