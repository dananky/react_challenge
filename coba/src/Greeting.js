import React from "react";

const greetings = props => {
  return (
    <div className="text-center">
      <ul className="list-group">
        <li className="list-group-item bg-dark">Nama : {props.name}</li>
        <li className="list-group-item bg-dark">Age : {props.age}</li>
        <li className="list-group-item bg-dark">Gender : {props.gender}</li>
      </ul>
    </div>
  );
};

greetings.defaultProps = {
  name: "undifined",
  age: "undifined",
  gender: "undifined"
};

export default greetings;
