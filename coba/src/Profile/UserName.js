import React from "react";

const username = (props) => (
    <h2 className="font-weight-bold">Halo, {props.user}</h2>
)

username.defaultProps = {
    user: "undifined"
};

export default username;