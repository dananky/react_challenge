import React from "react";
import logo from "../logo.svg";
import "../App.css";

const avatar = () => {
  return (
    <div>
      <img src={logo} className="App-logo" alt="logo" />
    </div>
  );
};

export default avatar;
