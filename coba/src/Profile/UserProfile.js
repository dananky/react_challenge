import React from "react";
import Avatar from "./Avatar";
import UserName from "./UserName";
import Bio from "./Bio";

const userprofile = () => {
  return (
    <div>
      <Avatar />
      <UserName user="Nanda"/>
      <Bio />
    </div>
  );
};

export default userprofile;
